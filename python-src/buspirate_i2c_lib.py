'''
Description :
 Bus Pirate Functions defines a class of a bus pirate along with methods to 
 exercise it.

Use as a library
 Instantiate a class of buspirate using <instance = clBP(COMPORT)>.  instance.<TAB> to list methods

Use as a script
 run buspirate_functions.py to set voltage on Nimbus BCC
Switches
 -verbose : print out readback from serial port
'''

__author__  = "David Bleloch"
__version__ = "0.1"
__date__ = "11/05/2014"

BUS_PIRATE_COM_PORT = 4

showdebugmessages = False

import sys
import time
import serial

# ==================================================================================================

def debug_msg(stringy):
    if (showdebugmessages == True) :
        print stringy

# ==================================================================================================

class clBP:

    def __init__(self, comport=BUS_PIRATE_COM_PORT):
        '''
        
    An instance of a bus pirate

        '''
       
        self.comport = comport
        self.device = 'buspirate'
            
        # configure the serial connections (the parameters differs on the device you are connecting to)
        self.ser = serial.Serial(
            port=comport-1,
            baudrate=115200,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS
        )
        
        time.sleep(0.1)
        
        self.reset()
        
        time.sleep(2)
        
        self.__setup_I2C_mode()
        
    # ==================================================================================================        
        
    def write_quick(self, addr):
        '''

    Send only the read / write bit 
    
        '''

        write_addr = addr<<1    
        
    # ==================================================================================================        
        
    def read_byte(self, addr):
        '''
        
    Read a single byte from a device, without specifying a device register. 
    
        '''
        
        read_addr = (addr<<1)+1
        
    # ==================================================================================================        
        
    def write_byte(self, addr, val):
        '''

    Send a single byte to a device 

        '''
    
        write_addr = addr<<1
    
        returnstring = self.__writeline('[' + hex(write_addr) + ' ' + hex(val) + ']' + '\n')
        
        debug_msg(returnstring)
        
    # ==================================================================================================        
        
    def read_byte_data(self, addr, cmd):
        '''

    Read Byte Data transaction. 

        '''
        
        read_addr = (addr<<1)+1
        
    # ==================================================================================================        
        
    def write_byte_data(self, addr, cmd, val):
        '''

    Write Byte Data transaction. 

        '''
     
        write_addr = addr<<1
    
    # ==================================================================================================        
        
    def read_word_data(self, addr, cmd):
        '''

    Read Word Data transaction. 

        '''
        
        read_addr = (addr<<1)+1
    
    # ==================================================================================================        
        
    def write_word_data(self, addr, cmd, val):
        '''

    Write Word Data transaction. 

        '''
        
        write_addr = addr<<1
    
    # ==================================================================================================        
        
    def process_call(self, addr, cmd, val):
        '''

    Process Call transaction. 
    
        '''
        
    # ==================================================================================================        
        
    def read_block_data(self, addr, cmd):
        '''

    Read Block Data transaction.      

        '''
        
        read_addr = (addr<<1)+1
    
    # ==================================================================================================        
        
    def write_block_data(self, addr, cmd, vals):
        '''
    
    Write up to 32 bytes to a device.
    This fucntion adds an initial byte indicating the length of the 
    vals array before the valls array.
    Use write_i2c_block_data instead!
    
        '''

    # ==================================================================================================        
        
    def block_process_call(self, addr, cmd, vals):
        '''

    Block Process Call transaction.      
    
        '''

    # ==================================================================================================        
        
    def read_i2c_block_data(self, addr, cmd):
        '''

    Block Read transaction. 

        '''
        
        read_addr = (addr<<1)+1
    
    # ==================================================================================================        
        
    def write_i2c_block_data(self, addr, cmd, vals):
        '''

    Block Write transaction. 

        '''
  
    # ==================================================================================================
        
    def __writeline(self, stringy, checktext = ''):
        '''
        
    Write a line of text to the bus pirate and get back any return chars
    
        '''
        rc = []
        self.ser.write(stringy + '\n')
        out = ''
        # let's wait one second before reading output (let's give device time to answer)
#        time.sleep(0.05)
#        while self.ser.inWaiting() > 0:
#            out += self.ser.read(1)
#
#        rc.append(out)
#        if out != '':
#            if checktext != '':
#                # check that the return string contains the check text
#                if out.find(checktext) < 0:
#                    rc.append ("CHECK STRING NOT FOUND WITHIN RETURN STRING")
#                else:
#                    rc.append ("PASSED")
#        else:
#            return "COMMUNICATIONS FAILED"
            
        return rc
        
    # ==================================================================================================
        
    def __setup_I2C_mode(self):
        '''
        
    Write the required chars to the bus pirate to set up I2C mode
    
        '''
        
        rc = []
        
        returnstring = self.__writeline('m')
        debug_msg(returnstring)
        time.sleep(0.5)
        returnstring = self.__writeline('4')            
        debug_msg(returnstring)
        time.sleep(0.5)
        returnstring = self.__writeline('3')
        debug_msg(returnstring)
        time.sleep(0.5)
        
        return rc

    # ==================================================================================================
        
    def reset(self):
        '''
        
    Resets the bus pirate
    
        '''
        rc = []
        
        returnstring = self.__writeline('#', 'Pirate')                
        time.sleep(1)
        debug_msg('Resetting...')
        debug_msg(returnstring)
        rc.append(returnstring)
        rc.append("SPI MODE SUCCESSFULLY RESET")
        return rc
            
    # ==================================================================================================

