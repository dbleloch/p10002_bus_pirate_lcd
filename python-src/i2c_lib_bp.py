#import buspirate_i2c_lib as bus_lib
import time

# http://www.raspberrypi.org/forums/viewtopic.php?f=32&t=34261&p=408195

# Instantiate an i2c device with a specific address, attached to the buspirate
class clI2C_Device:
   def __init__(self, addr, bp):
      self.addr = addr
      self.bus = bp

# Write a single command
   def write_cmd(self, cmd):
      self.bus.write_byte(self.addr, cmd)
      time.sleep(0.0001)
      
# Write a command and argument
   def write_cmd_arg(self, cmd, data):
      self.bus.write_byte_data(self.addr, cmd, data)
      time.sleep(0.0001)

# Write a block of data
   def write_block_data(self, cmd, data):
      self.bus.write_block_data(self.addr, cmd, data)
      time.sleep(0.0001)

# Read a single byte
   def read(self):
      return self.bus.read_byte(self.addr)

# Read 
   def read_data(self, cmd):
      return self.bus.read_byte_data(self.addr, cmd)

# Read a block of data
   def read_block_data(self, cmd):
      return self.bus.read_block_data(self.addr, cmd)