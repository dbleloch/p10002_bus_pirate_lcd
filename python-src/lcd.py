import buspirate_i2c_lib as bp_lib
import lcddriver
from time import *

# http://www.raspberrypi.org/forums/viewtopic.php?f=32&t=34261&p=408195

# instantiate a bus pirate to tell the code where the i2c port is located
bp = bp_lib.clBP()

# instantiate an lcd and pass it the bus-pirate instance
# "this is my lcd, it's attached to the bus pirate and it has the address 27h"
lcd = lcddriver.clLCD(bp, 0x27)

# write to lcd display < lcd_display_string(string, line) >
lcd.lcd_display_string("ABCD", 1)
lcd.lcd_display_string("1234", 2)
lcd.lcd_display_string("4x20 LCD Screen", 3)
lcd.lcd_display_string("Ready", 4)