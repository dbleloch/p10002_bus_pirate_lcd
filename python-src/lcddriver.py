import i2c_lib_bp as i2c_lib
#import i2c_lib_dummy as i2c_lib
import time

# http://www.raspberrypi.org/forums/viewtopic.php?f=32&t=34261&p=408195

# commands
LCD_CLEARDISPLAY = 0x01
LCD_RETURNHOME = 0x02
LCD_ENTRYMODESET = 0x04
LCD_DISPLAYCONTROL = 0x08
LCD_CURSORSHIFT = 0x10
LCD_FUNCTIONSET = 0x20
LCD_SETCGRAMADDR = 0x40
LCD_SETDDRAMADDR = 0x80

# flags for display entry mode
LCD_ENTRYRIGHT = 0x00
LCD_ENTRYLEFT = 0x02
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
LCD_DISPLAYON = 0x04
LCD_DISPLAYOFF = 0x00
LCD_CURSORON = 0x02
LCD_CURSOROFF = 0x00
LCD_BLINKON = 0x01
LCD_BLINKOFF = 0x00

# flags for display/cursor shift
LCD_DISPLAYMOVE = 0x08
LCD_CURSORMOVE = 0x00
LCD_MOVERIGHT = 0x04
LCD_MOVELEFT = 0x00

# flags for function set
LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00
LCD_2LINE = 0x08
LCD_1LINE = 0x00
LCD_5x10DOTS = 0x04
LCD_5x8DOTS = 0x00

# flags for backlight control
LCD_BACKLIGHT = 0x08
LCD_NOBACKLIGHT = 0x00

Bl = 0b00001000 # Backlight bit
En = 0b00000100 # Enable bit
Rw = 0b00000010 # Read/Write bit
Rs = 0b00000001 # Register select bit

class clLCD:
   #initializes objects and lcd
   def __init__(self, i2c_bus_device, address):
      self.lcd_device = i2c_lib.clI2C_Device(address, i2c_bus_device)
      
      self.i2c_bus_device = i2c_bus_device # this is where the i2c bus resides (only works as a buspirate currently)

      # set the lcd to 4 bit mode
      self.lcd_write_single(0x30)
      time.sleep(0.1)
      self.lcd_write_single(0x30)
      time.sleep(0.01)
      self.lcd_write_single(0x30)
      time.sleep(0.01)
      self.lcd_write_single(0x20)
      time.sleep(0.01)
      # then set up the function and display options
      self.lcd_write_double(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE)
      time.sleep(0.01)
      self.lcd_write_double(LCD_DISPLAYCONTROL | LCD_DISPLAYON | LCD_CURSOROFF)
      time.sleep(0.01)
      self.lcd_write_double(LCD_CLEARDISPLAY)   
      time.sleep(0.01)
      self.lcd_write_double(LCD_ENTRYMODESET | LCD_ENTRYLEFT)
      time.sleep(0.02)   

   # clocks EN to latch command
   def lcd_strobe(self, data, checkbp=False):
      self.lcd_device.write_cmd(((data & ~En) | Bl))
      time.sleep(.01) 
      self.lcd_device.write_cmd(data | En | Bl)
      time.sleep(.01)
      self.lcd_device.write_cmd(((data & ~En) | Bl))
      time.sleep(.01)   
#      if checkbp == True:
#        while not (self.lcd_device.read_cmd() & 0x80)
        
 

   # write a command to lcd
   def lcd_write_double(self, data, mode=0):
      # write high nibble
      high_nibble_data = (mode | (data & 0xF0))
      self.lcd_strobe(high_nibble_data)
      # write low nibble
      low_nibble_data = (mode | ((data << 4) & 0xF0))
      self.lcd_strobe(low_nibble_data)

   # single transaction instruction write to lcd
   def lcd_write_single(self, data):
      # write high nibble
      self.lcd_strobe(data)
      
   # put string function
   def lcd_display_string(self, string, line):
      if line == 1:
         self.lcd_write_double(0x80)
      if line == 2:
         self.lcd_write_double(0xC0)
      if line == 3:
         self.lcd_write_double(0x94)
      if line == 4:
         self.lcd_write_double(0xD4)

      for char in string:
         self.lcd_write_double(ord(char), Rs)

   # clear lcd and set to home
   def lcd_clear(self):
      self.lcd_write_double(LCD_CLEARDISPLAY)
      self.lcd_write_double(LCD_RETURNHOME)